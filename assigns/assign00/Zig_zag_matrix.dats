(* ****** ****** *)

(*
//
// Here is a C program
//
// http://rosettacode.org/wiki/Zig_zag_matrix#C
//
*)

(* ****** ****** *)
//
// HX: 10 points
//
(* ****** ****** *)
//
extern
fun
Zig_zag_matrix(n: int): void
//
(* ****** ****** *)

(* end of [Zig_zag_matrix.dats] *)

